# nevha.registry

NevHA's registry service is responsible for tracking not only devices, but their states.

As NevHA's goal, like any other home automation harnesses, is it make the many different home automation APIs accessible via one, standard, 'stateless' API.

Stateless is quoted as all HA devices have a state, such as a light bulb being on at 50% brightness. This 'device state' is kept up to date by the 