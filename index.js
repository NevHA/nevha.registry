// NevHA Device Registry Service
// Copyright Cameron Fleming (Nevexo) 2019
// Licensed under the MIT License.

const Winston = require("winston")

const logger = Winston.createLogger({
    level: process.env.NEVHA_LOG,
    format: Winston.format.json(),
    transports: [
        new Winston.transports.File({ filename: 'logs/error.log', level: 'error' }),
        new Winston.transports.File({ filename: 'logs/combined.log' })
    ]
});
// If not in production mode, switch the format to simple.
if (process.env.NEVHA_MODE !== 'production') {
    logger.add(new Winston.transports.Console({
        format: Winston.format.simple()
    }));
}

