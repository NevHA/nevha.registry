#!/bin/bash
# Setup dev envs for this module of NevHA
echo 'Configuring...'
# NEVHA_MODE - Set logging level & database details.
export NEVHA_MODE='dev'
# NEVHA_LOG - Set logging level
export NEVHA_LOG='debug'
echo 'Done.'